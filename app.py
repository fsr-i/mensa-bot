
#!/usr/bin/env python3

import pprint
pp = pprint.PrettyPrinter(indent=2).pprint

import asyncio
from PIL import Image
from nio import (AsyncClient, ClientConfig, RoomMessageText, InviteEvent)
import requests

import re, yaml, sqlite3, magic, random, glob, functools
import os, io, sys, signal
import datetime
dir_path = os.path.dirname(os.path.realpath(__file__))

import parse as MensaParser
from xml.dom.minidom import parseString as parseXML


config = yaml.load(open("config.yaml"), Loader=yaml.SafeLoader)
bot = AsyncClient(
	config['homeserver_url'],
	config['username']#,
	#device_id=config['device_id'],
	#store_path=dir_path,
	#config=ClientConfig(store_sync_tokens=True)
)

CANTEEN_DEFAULT = config['default_canteen'] if 'default_canteen' in config.keys() else 'goerlitz'
PARSER = config['parser'] if 'parser' in config.keys() and config['parser'] in MensaParser.parsers.keys() else 'dresden'


sub_db = sqlite3.connect('subscriptions.db')

def db_cmd(cmd, data=tuple()):
	out = sub_db.execute(cmd, data)
	sub_db.commit()
	return tuple(out)

def db_init():
	return (
		db_cmd('CREATE TABLE IF NOT EXISTS subscriptions (room VARCHAR(96) NOT NULL, mensa VARCHAR(32) NOT NULL, PRIMARY KEY (room, mensa));') or
		db_cmd('CREATE TABLE IF NOT EXISTS push (ts DATETIME PRIMARY KEY);')
	)

def db_sub(room, mensa):
	return db_cmd('INSERT INTO subscriptions (room, mensa) VALUES (?, ?);', (room, mensa))

def db_unsub(room, mensa):
	return db_cmd('DELETE FROM subscriptions WHERE room=? AND mensa=?;', (room, mensa))

def db_unsub_all(room):
	return db_cmd('DELETE FROM subscriptions WHERE room=?;', (room,))

def db_get_subs(room):
	return db_cmd('SELECT mensa FROM subscriptions WHERE room=?;', (room,))

def db_get_all():
	return db_cmd('SELECT room, mensa FROM subscriptions;')

def db_register_push():
	return (
		db_cmd('INSERT INTO push (ts) VALUES (?);', (datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),)) and
		db_cmd('DELETE FROM push WHERE ts <= date("now", "-8 day");')
	)

def db_get_last_push():
	return db_cmd('SELECT ts FROM push ORDER BY ts DESC LIMIT 1;')



async def send_msg(room_id, content, plain=None, notice=False):
	opt = {"msgtype": "m.text" if not notice else "m.notice"}
	if plain:
		opt["body"] = plain
		opt["format"] = "org.matrix.custom.html"
		opt["formatted_body"] = content
	else:
		opt["body"] = content
	return await bot.room_send(room_id, 'm.room.message', opt)



async def process_all_subs():
	db_register_push()
	
	subs = db_get_all()
	
	def get_canteens(out, t):
		if t[1] not in out:
			out.append(t[1])
		return out
	
	def populate_with_plans(out, canteen):
		out[canteen] = formatMensa(
			getToday(canteen),
			writePlain=True,
			writeHtml=True
		)
		return out
	
	individual_canteens = functools.reduce(get_canteens, subs, list())
	individual_canteens = functools.reduce(populate_with_plans, individual_canteens, dict())
	
	def group_rooms(out, t):
		if t[0] in out.keys():
			out[t[0]].append(t[1])
		else:
			out[t[0]] = [t[1]]
		return out
	
	subs = functools.reduce(group_rooms, subs, dict())
	
	for room_id, canteens in subs.items():
		content = {'plain': '', 'html': ''}
		
		for canteen in canteens:
			content['plain'] += individual_canteens[canteen]['plain']+'\n'
			content['html'] += individual_canteens[canteen]['html']+'\n'
		
		await send_msg(room_id, content['html'].strip(), content['plain'].strip()) and print('  m.room.message to '+room_id+' (process_all_subs(): '+str(canteens)+') on my own')



def getMensaFeed(canteen=None):
	if not canteen:
		canteen = CANTEEN_DEFAULT
	return parseXML(
		MensaParser.parse(
			MensaParser.SimulatedRequest(),
			PARSER, canteen, 'full.xml'
		)
	)

def falseTodayISO():
	# don't actually get today, but the smartest choice - 6 hours before midnight, we're probably asking about tomorrow...
	six_hours = 6*60*60
	return falseISO(six_hours)

def falseTomorrowISO():
	# don't actually get today, but the smartest choice - 3 hours after midnight, we're probably asking about today...
	twentyfour_hours = 24*60*60
	three_hours = 3*60*60
	return falseISO(twentyfour_hours - three_hours)
	
def falseISO(offset):
	now = datetime.datetime.now()
	false_ts = datetime.datetime.fromtimestamp(now.timestamp() + offset)
	return false_ts.strftime('%Y-%m-%d')


def getToday(canteen=None):
	return getDate(falseTodayISO(), canteen)

def getTomorrow(canteen=None):
	return getDate(falseTomorrowISO(), canteen)

def getDate(ISOdate, canteen=None):
	if not canteen:
		canteen = CANTEEN_DEFAULT
	feed = getMensaFeed(canteen)
	days = feed.getElementsByTagName('day')
	day_list = filter(lambda day: day.attributes.get('date').value == ISOdate, days)
	return (canteen, list(day_list))

def formatMensa(canteens, writePlain=True, writeHtml=False):
	if not canteens:
		return 'Ich konnte nichts dazu finden.'
		
	if type(canteens) is tuple:
		canteens = {canteens[0]: canteens[1]}
	
	out = {'plain': '', 'html': ''}
	
	def plain(string):
		if writePlain:
			out['plain'] += string
	
	def html(string):
		if writeHtml:
			out['html'] += string
	
	def both(string):
		plain(string)
		html(string)
		
	emoji = {
		"Utensils": "🍴",
		
		"enthält Rindfleisch":		"🐄",
		"enthält Schweinefleisch":	"🐖",
		"enthält Knoblauch":		"🧄",
		"enthält Alkohol":			"🍷",
		"vegetarisch":				"🍅",
		"vegan":					"🌱",
		
		"student":					"👩‍🎓",	#"👨‍🎓",
		"employee":					"👤"
	}
		
	for canteen, dom in canteens.items():
		for day in dom:
			if canteen == 'goerlitz':
				canteen = 'Görlitz'
			else:
				canteen = canteen[0].upper()+canteen[1:]
			
			if len(out['plain'] or out['html']) > 0:
				both('\n')
			#date = datetime.datetime.fromisoformat(day.attributes.get('date').value)
			# .fromisoformat is not available in python 3.6.*, UGH. Fine, let's parse it ourselves...
			date = datetime.datetime.strptime(day.attributes.get('date').value, '%Y-%m-%d')
			html('<h4>')
			both(emoji['Utensils']+' '+canteen+date.strftime(' %a, %d.%m. (KW %W):'))
			categories = day.getElementsByTagName('category')
			if len(categories) > 0:
				html('</h4>')
				both('\n')
				for category in categories:
					if category.nodeName == '#text':
						continue
					both(' '+category.attributes.get('name').value+':\n')
					html(' <ul>\n')
					for meal in category.getElementsByTagName('meal'):
						if meal.nodeName == '#text':
							continue
						plain(' - ')
						html(' <li>')
						both(meal.getElementsByTagName('name')[0].childNodes[0].data+'\n')
						html('   <br>')
						html('   '+', '.join(
							list(map(
								lambda note: (lambda data: emoji[data] if data in emoji.keys() else data)(note.childNodes[0].data),
								meal.getElementsByTagName('note')
							))
						)+' <b>|</b> ')
						plain('   '+', '.join(
							list(map(
								lambda note: (lambda data: emoji[data]+' '+data if data in emoji.keys() else data)(note.childNodes[0].data),
								meal.getElementsByTagName('note')
							))
						)+' | ')
						
						html(', '.join(
							list(map(
								lambda price: (lambda who: emoji[who] if who in emoji.keys() else who)(price.attributes.get('role').value)+' '+price.childNodes[0].data+' €',
								meal.getElementsByTagName('price')
							))
						))
						plain(', '.join(
							list(map(
								lambda price: (lambda who: emoji[who]+' '+who if who in emoji.keys() else who)(price.attributes.get('role').value)+' '+price.childNodes[0].data+' €',
								meal.getElementsByTagName('price')
							))
						))
						html(' </li>')
						both('\n')
					html(' </ul>\n')
			else:
				both(' kein Angebot')
				html('</h4>')
	
	out['plain'] = out['plain'].strip()
	out['html'] = out['html'].strip()
	return out


date_regex = re.compile('^20(19|[23][0-9])-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])')


async def message_cb(room, event):
	if not event.sender.startswith('@'+config['username']+':'):
		args = event.body.split()
		isitme = tuple(filter(lambda x: x.startswith('@'+config['username']+':') or x == '!mensa', args))
		args = tuple(filter(lambda x: x not in isitme, args))
		if len(isitme) > 0:
			print("oh, it's my turn: "+str(args))
			args = list(map(lambda a: a.lower(), args))
			
			if len(args) > 0:
				#if args[0] == 'id':
				#	await send_msg(room.room_id, 'Room ID: '+room.room_id+('(debug_room)' if room.room_id == config['debug_room'] else ''))
				#	return
				#if args[0] == 'exception':
				#	await sendmsg(room.room_id, 'Generating Exception...')
				#	raise Exception("Fake exception, generated by "+event.sender+" in "+room.room_id)
				#	return
				if args[0] == 'help':
					out = 'Usage: \t!mensa \t\t\t\t\t Get todays menu for default canteen\n'
					out += '\t!mensa <canteens and/or dates> Show menu for specified canteens at the specified dates in ISO format (YYYY-MM-DD) or "today" and "tomorrow".\n'
					out += '\t!mensa sub <canteen(s)> \t\t Subscribe room to canteen(s) to get notified of the menu every weekday at 10:45\n'
					out += '\t!mensa unsub <canteen(s)> \t Unsubscribe from canteen(s). Specify "all" to remove all subscriptions for the room\n'
					out += '\t!mensa subs \t\t\t\t\t List all subscriptions for this room\n'
					out += '\t!mensa canteens \t\t\t\t List all canteens available with the parser "'+PARSER+'"\n'
					out += '\t!mensa help \t\t\t\t\t Show this help\n'
					out += ' Default canteen: "'+config['default_canteen']+'"'
					await send_msg(room.room_id, out)
					return
				if args[0] == 'canteens' or args[0] == 'mensen':
					out = "Available canteens: "+', '.join(MensaParser.parsers[PARSER].sources.keys())
					await send_msg(room.room_id, out)
					return
				if args[0] == 'subs' or args[0] == 'subscriptions':
					subs = db_get_subs(room.room_id)
					if len(subs) > 0:
						out = "This room is subscribed to "
						out += ', '.join(map(lambda d: d[0], subs))
					else:
						out = "This room is not subscribed to any canteens"
						
					await send_msg(room.room_id, out, notice=True) and print('  m.room.message to '+room.room_id+' (message_cb() SUBS: args: '+str(args)+') from '+event.sender)
					return
				if args[0] == 'sub' or args[0] == 'subscribe':
					canteens = args[1:]
					if len(canteens) == 0:
						canteens.append(CANTEEN_DEFAULT)
					
					already = list()
					for canteen in canteens:
						try:
							db_sub(room.room_id, canteen)
						except sqlite3.IntegrityError:
							already.append(canteen)
					
					diff = set(canteens).difference(already)
					content = list()
					if len(diff) > 0:
						content.append("Subscribed to "+', '.join(diff))
					if len(already) > 0:
						content.append("Already subscribed to "+', '.join(already))
					await send_msg(room.room_id, '\n'.join(content), notice=True) and print('  m.room.message to '+room.room_id+' (message_cb() SUB: args: '+str(args)+') from '+event.sender)
					return
				if args[0] == 'unsub' or args[0] == 'unsubscribe':
					canteens = args[1:]
					if len(canteens) == 0:
						canteens.append(CANTEEN_DEFAULT)
						
					if 'all' in canteens or 'any' in canteens:
						db_unsub_all(room.room_id)
						await send_msg(room.room_id, 'Unsubscribed from any canteens', notice=True) and print('  m.room.message to '+room.room_id+' (message_cb() UNSUB ALL: args: '+str(args)+') from '+event.sender)
					else:
						already = list()
						for canteen in canteens:
							try:
								db_unsub(room.room_id, canteen)
							except sqlite3.IntegrityError:
								already.append(canteen)
						
						diff = set(canteens).difference(already)
						content = list()
						if len(diff) > 0:
							content.append("Unsubscribed from "+', '.join(diff))
						if len(already) > 0:
							content.append("Already unsubscribed from "+', '.join(already))
						await send_msg(room.room_id, '\n'.join(content), notice=True) and print('  m.room.message to '+room.room_id+' (message_cb() UNSUB: args: '+str(args)+')')
					return
			
			feed = dict()
			canteens = list()
			dates = list()
			if 'görlitz' in args or 'goerlitz' in args:
				canteens.append('goerlitz')
			available_canteens = MensaParser.parsers[PARSER].sources.keys()
			for a in args:
				if a in available_canteens:
					canteens.append(a)
			if not canteens:
				canteens.append(None)
			
			if 'tomorrow' in args or 'morgen' in args:
				#dates.append(getTomorrow)
				args[args.index('tomorrow') if 'tomorrow' in args else args.index('morgen')] = falseTomorrowISO()
			if 'today' in args or 'heute' in args:
				#dates.append(getToday)
				args[args.index('today') if 'today' in args else args.index('heute')] = falseTodayISO()
			
			dates = set(filter(date_regex.match, args))
			if not dates:
				dates.add(falseTodayISO())
			dates = sorted(dates)
			
			for canteen in canteens:
				for date in dates:
					c = getDate(date, canteen)
					if c[0] in feed:
						feed[c[0]].extend(c[1])
					else:
						feed[c[0]] = c[1]
			
			content = formatMensa(feed, writePlain=True, writeHtml=True)

			await send_msg(room.room_id, content['html'], content['plain']) and print('  m.room.message to '+room.room_id+' (message_cb() default: args: '+str(args)+') from '+event.sender)
			return

		print('    [ boring message by '+event.sender+' ]')


async def catcher(fn, *args, **kwargs):
	try:
		await fn(*args, **kwargs)
	except Exception as e:
		print('EXCEPTION!!!', e)
		if config['debug_room']:
			try:
				await send_msgd(config['debug_room'], 'EXCEPTION!!! '+str(e))
			except Exception as e:
				print('couldn\'t report Exception to '+config['debug_room']+' because:', e)





async def invite_cb(room, event):
	print(
		"{} invited me to room {}! (I think)".format(
			room.user_name(event.sender), room.display_name
		)
	)
	await bot.join(room.room_id)


bot.add_event_callback(lambda *a, **kw: catcher(message_cb, *a, **kw), RoomMessageText)
bot.add_event_callback(lambda *a, **kw: catcher(invite_cb, *a, **kw), InviteEvent)


async def scheduler():
	days = ('mon','tue','wed','thu','fri')
	target_time = datetime.time(hour=10, minute=45)
	cooldown = datetime.timedelta(hours=18)
	
	while True:
		await asyncio.sleep(5*60)
		now = datetime.datetime.now()
		if now.time() > target_time and now.strftime('%a').lower() in days:
			last = db_get_last_push()
			if type(last) is tuple and len(last) > 0:
				last = datetime.datetime.strptime(last[0][0], '%Y-%m-%d %H:%M:%S')
				if last + cooldown > now:
					# less than cooldown between last and now, don't trigger again
					continue
			# out with it!
			await process_all_subs()

async def main():
	await bot.login(config['password'])
	await bot.sync_forever(timeout=30000, full_state=True)
	print('bot.sync_forever stopped on its own!')


try:
	db_init()
	event_loop = asyncio.get_event_loop()
	event_loop.run_until_complete(asyncio.wait([main(), scheduler()]))
except KeyboardInterrupt:
	# console just printed '^C', so omit the 'C'
	print('leaning up...')
except Exception as e:
	pp(e)
finally:
	sub_db.close()
	asyncio.get_event_loop().run_until_complete(bot.logout())
	asyncio.get_event_loop().run_until_complete(bot.close())
	sys.exit(0)
