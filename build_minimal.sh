#!/bin/sh

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"


function build() {
	if [ ".$1" == "." ]; then
		parser="dresden"
	else
		parser="$1"
	fi

	echo " building with only '$parser'..."

	cd "$DIR" &&
	mkdir build && cd build &&
	git clone --recurse-submodules https://github.com/mswart/openmensa-parsers.git &&

	echo " Copying only relevant bits (reducing to ~1/3 required space)" &&
	cd openmensa-parsers/ &&
	cp -r parse.py config.py utils.py pyopenmensa ../ &&
	cd ../ && mkdir parsers &&
	cp openmensa-parsers/parsers/__init__.py "openmensa-parsers/parsers/$parser.py" parsers/ &&
	patch_config "$parser" &&
	rm -rf openmensa-parsers/ &&

	cd $DIR &&
	echo " Adding our bot files..." &&
	cp app.py config.yaml.example requirements.txt build/ &&
	sed -e "s/^parser:.*$/parser: $parser/g" -i ./build/config.yaml.example &&

	echo "" &&
	echo "... Done! The contents of build/ can be deployed and started after installing the other python dependencies (requirements.txt)." || error "$parser"
}

function clean() {
	echo " Deleting previous build dir..." &&
	rm -rf "$DIR/build/"
}

function patch_config() {
	echo " Patching config.py to expect only the parser for '$1'" &&
	sed -e "s/^\s\+'.*',\?$/#&/g" -e "/^#\+\s\+'$1',\?/s/^#\+//g" -i ./config.py
}

function error() {
	echo "" &&
	echo "  ERROR! Something went wrong, build aborted. Did you write the name of your parser to include correctly? ('$1')"
}


clean &&
build "$1"
